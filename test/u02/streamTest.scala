package u02

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u03.Lists.List._
import u03.Streams._

class streamTest {

  @Test def testDrop(): Unit ={
    val s = Stream.take(Stream.iterate(0)(_+1))(10)
    assertEquals(Stream.toList(Stream.drop(s)(6)), Cons(6, Cons(7, Cons(8, Cons(9, Nil())))))
    assertEquals(Stream.toList(Stream.empty()), Stream.toList(Stream.drop(Stream.empty())(2)))
  }

  @Test def testConstants(): Unit ={
    val s = Stream.toList(Stream.take(Stream.constant("x"))(5))
    val s1 = Stream.toList(Stream.take(Stream.constant("x"))(0))
    assertEquals(Cons("x", Cons("x", Cons("x", Cons("x", Cons("x", Nil()))))), s)
    assertEquals(Nil(), s1)
  }

  @Test def testFibonacci(): Unit ={
    val s = Stream.toList(Stream.take(Stream.fibonacci())(8))
    val s1 = Stream.toList(Stream.take(Stream.fibonacci())(0))
    val s2 = Stream.toList(Stream.take(Stream.fibonacci())(1))
    assertEquals(Cons(0, Cons(1, Cons(1, Cons(2, Cons(3, Cons(5, Cons(8, Cons(13, Nil())))))))), s)
    assertEquals(Cons(0, Nil()), s2)
    assertEquals(Nil(), s1)
  }

}
