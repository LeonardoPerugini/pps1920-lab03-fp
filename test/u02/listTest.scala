package u02

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u02.SumTypes._
import u02.Modules.Person
import u02.Modules.Person._

import u03.Lists.List._

class listTest {

  @Test def dropTest(): Unit = {
    val lst = Cons(10, Cons(20, Cons(30, Nil())))
    assertEquals(Cons(20, Cons(30, Nil())), drop(lst, 1))
    assertEquals(Cons(30, Nil()), drop(lst, 2))
    assertEquals(Nil(), drop(lst, 5))
  }

  @Test def flatMapTest(): Unit = {
    val lst = Cons(10, Cons(20, Cons(30, Nil())))
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), flatMap(lst)(v => Cons(v + 1, Nil())))
    assertEquals(Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))), flatMap(lst)(v => Cons(v + 1, Cons(v + 2, Nil()))))
  }

  @Test def mapFlatTest(): Unit ={
    val lst = Cons(10, Cons(20, Cons(30, Nil())))
    //assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), map(lst)(v => v+1))
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), mapFlat(lst)(v => v+1))
  }

  @Test def filterFlatTest(): Unit ={
    val l = Cons(10, Cons(20, Cons(30, Nil())))
    assertEquals(Cons(20, Cons(30, Nil())), filterFlat[Int](l)(_ >=20))
  }

  import u02.Optionals.Option._

  @Test def maxTest(): Unit ={
    val lst = Cons(10, Cons(20, Cons(30, Nil())))
    val lst1 = Cons(50, Cons(10, Cons(40, Nil())))
    assertEquals(Some(30), max(lst))
    assertEquals(None(), max(Nil()))
    assertEquals(Some(50), max(lst1))
  }


  @Test def coursesTest(): Unit ={
    val t:Person = Person.Teacher("T1", "Storia")
    val t1:Person = Person.Teacher("T21", "Geografia")
    val s:Person = Person.Student("S1", 2000)
    val lst: Cons[Person] = Cons(t, Cons(s, Cons(t1, Nil())))
    assertEquals(Cons("Storia", Cons("Geografia", Nil())), getCourses(lst))
    assertEquals(Nil(), getCourses(Nil()))
  }

  @Test def foldTest(): Unit ={
    val lst = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
    assertEquals(-16, foldLeft(lst)(0)(_-_))
    assertEquals(-8, foldRight(lst)(0)(_-_))
  }

}
