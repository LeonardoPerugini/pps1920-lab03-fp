package u03

object Main {

  //part 3a
  def parity(n: Int): String = n%2 match {
    case 0 => "even"
    case _ => "odd"
  }
  val printParity: Int => String = {
    case n if n%2==0 => "even"
    case _ => "odd"
  }


  //part 3b
  val neg: (String => Boolean) => String => Boolean = p => s => !p(s)
  def neg2(func: String => Boolean): (String=>Boolean) = (string: String) => (!func(string))

  //part 3c
  def gNeg[A](f: A => Boolean): (A => Boolean) = (p: A) => !f(p)

  //part4
  val valCurried: Int => Int => Int => Boolean = x => y => z => (x<= y && y <= z)
  val valNotCurried: ( Int, Int, Int ) => Boolean = (x, y, z) => (x <= y && y <= z)
  def defCurried(x: Int)(y: Int)(z: Int): Boolean = (x, y, z)  match {
    case (x, y, z) if ((x<=y) && (y<=z)) => true
    case _ => false
  }
  def defNotCurried(x: Int, y: Int, z: Int): Boolean = (x, y, z) match {
    case (x, y, z) if ((x<=y) && (y<=z)) => true
    case _ => false
  }

  //part 5
  def compose(f: Int => Int, g: Int => Int): Int => Int = i => f(g(i))
  //println(compose(_-1, _*2)(5))

  // part 6
  def fibonacci(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => fibonacci(n-1) + fibonacci(n-2)
  }

  // part 7
  sealed trait Shape
  object Shape {
    case class Rectangle(b: Double, h: Double) extends Shape
    case class Circle(r: Double) extends Shape
    case class Square(l: Double) extends Shape

    def perimeter(s: Shape): Double = s match {
      case Rectangle(b, h) => 2*(b+h)
      case Circle(r) => 2*Math.PI*r
      case Square(l) => 4*l
    }

    def area(s: Shape): Double = s match {
      case Rectangle(b, h) => b*h
      case Circle(r) => Math.PI*(r*r)
      case Square(l) => l*l
    }
  }

  // part 8
  sealed trait Option[A]
  object Option {

    case class None[A]() extends Option[A]
    case class Some[A](a: A) extends Option[A]

    def isEmpty[A](opt: Option[A]): Boolean = opt match {
      case None() => true
      case _ => false
    }

    def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
      case Some(a) => a
      case _ => orElse
    }

    def flatMap[A, B](opt: Option[A])(f: A => Option[B]): Option[B] = opt match {
      case Some(a) => f(a)
      case _ => None()
    }

    def filter[A](opt: Option[A])(f: Option[A] => Boolean): Option[A] = opt match {
      case opt if (!Option.isEmpty(opt) && f(opt)) => opt
      case _ => None()
    }

    def map[A](opt: Option[A])(f:Option[A] => Boolean): Option[Boolean] = opt match {
      case opt if !isEmpty(opt) => Some(f(opt))
      case _ => None()
    }

    def map2[A](opt1: Option[A], opt2: Option[A])(f: (Option[A], Option[A]) => Option[A]): Option[A] = (opt1, opt2) match {
      case _ if (opt1 == None() || opt2 == None()) => None()
      case _ => f(opt1, opt2)
    }
  }
}
